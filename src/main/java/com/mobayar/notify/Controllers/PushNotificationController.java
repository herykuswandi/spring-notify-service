package com.mobayar.notify.Controllers;


import com.mobayar.notify.Models.PushNotification;
import com.mobayar.notify.Repositories.PushNotificationRepository;
import com.mobayar.notify.Services.PushNotificationService;
import com.mobayar.notify.Utils.AppConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import java.util.Date;

@RestController
@Api(value = "Push Notification", description = "Push Notification Service", tags = {"Push Notification"})
@RequestMapping("push-notification")
public class PushNotificationController {
    @Autowired
    private PushNotificationService pushNotificationService;

    @Autowired
    private PushNotificationRepository pushNotificationRepository;


    @PostMapping("send")
    public ResponseEntity<PushNotification> send(@RequestParam @ApiParam(value = "Recipient (Use Comma Separated for Multiple or segments \"All\", \"Active Users\", \"Inactive Users\")", required = true) String recipient,
                                                 @RequestParam String title,
                                                 @RequestParam String body,
                                                 @RequestParam(required = false) @ApiParam(value = "Type (Example : transaction, broadcast)",required=false) String type,
                                                 @RequestParam(required = false) @ApiParam(value = "String Json (Example : {\"action_type:info,id:2938021\"})",required = false) String data) {

        try {
            PushNotification pushNotification = new PushNotification();
            pushNotification.setRecipient(recipient);
            pushNotification.setTitle(title);
            pushNotification.setBody(body);
            pushNotification.setType(type);
            pushNotification.setData(data);
            pushNotification.setStatus(AppConstant.PENDING);
            pushNotification.setCreatedAt(new Date());
            pushNotificationRepository.save(pushNotification);

            if (pushNotification != null) {
                pushNotificationService.send(pushNotification.getId());
                return new ResponseEntity(pushNotification, HttpStatus.OK);
            } else {
                throw new RestClientException("Cant create push notification history");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
