package com.mobayar.notify.Controllers;

import com.mobayar.notify.Models.Email;
import com.mobayar.notify.Repositories.EmailRepository;
import com.mobayar.notify.Services.MailService;
import com.mobayar.notify.Utils.AppConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;

import java.util.Date;

@RestController
@Api(value = "Email", description = "Email Service", tags = {"Email"})
@RequestMapping("email")
public class EmailController {
    @Autowired
    private MailService mailService;

    @Autowired
    private EmailRepository emailRepository;


    @PostMapping("send")
    public ResponseEntity<Email> send(@RequestParam @ApiParam(value = "Recipient (Use Comma Separated for Multiple)",required=true) String recipient,
                                      @RequestParam String subject,
                                      @RequestParam @ApiParam(allowMultiple =true,required=true) String body,
                                      @RequestParam(required = false) @ApiParam(value = "Type (Example : transaction, report, daily_report)",required=false)  String type) {

        try {
            Email email = new Email();
            email.setRecipient(recipient);
            email.setSubject(subject);
            email.setBody(body);
            email.setType(type);
            email.setStatus(AppConstant.PENDING);
            email.setCreatedAt(new Date());
            emailRepository.save(email);
            if (email != null) {
                mailService.send(email.getId());
                return new ResponseEntity(email, HttpStatus.OK);
            } else {
                throw new RestClientException("Cant create email history");
            }

        } catch (MailException e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
