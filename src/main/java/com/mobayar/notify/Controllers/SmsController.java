package com.mobayar.notify.Controllers;


import com.mobayar.notify.Models.Sms;
import com.mobayar.notify.Repositories.SmsRepository;
import com.mobayar.notify.Services.SmsService;
import com.mobayar.notify.Utils.AppConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import java.util.Date;

@RestController
@Api(value = "Sms", description = "Sms Service", tags = {"Sms"})
@RequestMapping("sms")
public class SmsController {
    @Autowired
    private SmsService smsService;

    @Autowired
    private SmsRepository smsRepository;


    @PostMapping("send")
    public ResponseEntity<Sms> send(@RequestParam String recipient,
                                    @RequestParam String message,
                                    @RequestParam  @ApiParam(value = "Type (promotion,alert,otp,other)", example ="other" ,required=true) String type) {

        try {
            Integer channel;
            String batch;
            String uploadBy;

            switch (type.toLowerCase()) {
                case "promotion":
                    channel = 0;
                    batch = "promotion";
                    uploadBy = "promotion_system";
                    break;
                case "alert":
                    channel = 1;
                    batch = "alert";
                    uploadBy = "alert_system";
                    break;
                case "otp":
                    channel = 2;
                    batch = "otp";
                    uploadBy = "otp_system";
                    break;
                default:
                    channel = 0;
                    batch = "other";
                    uploadBy = "system";
                    break;
            }

            Sms sms = new Sms();
            sms.setRecipient(recipient);
            sms.setMessage(message);
            sms.setType(type);
            sms.setDivision("IT");
            sms.setChannel(channel);
            sms.setBatchname(batch);
            sms.setUploadBy(uploadBy);
            sms.setStatus(AppConstant.PENDING);
            sms.setCreatedAt(new Date());


            smsRepository.save(sms);
            if (sms != null) {
                smsService.send(sms.getId());
                return new ResponseEntity(sms, HttpStatus.OK);
            } else {
                throw new RestClientException("Cant create sms history");
            }

        } catch (MailException e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
