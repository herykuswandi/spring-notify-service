package com.mobayar.notify.Services;

import com.mobayar.notify.Models.Sms;
import com.mobayar.notify.Repositories.SmsRepository;
import com.mobayar.notify.Utils.AppConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class SmsService {

    @Value("${sms.userid}")
    private String sms_userid;

    @Value("${sms.password}")
    private String sms_password;

    @Value("${sms.sender}")
    private String sms_sender;

    @Autowired
    private SmsRepository smsRepository;

    @Async
    public void send(String id) {

        Sms sms = smsRepository.findById(id).get();
        try {
            URL url = new URL("https://sms-api.jatismobile.com/index.ashx?division=IT&batchname=other&uploadby=system&chan2nel=0");
            HttpURLConnection con = null;
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            Map<String, String> parameters = new HashMap<>();
            parameters.put("userid", sms_userid);
            parameters.put("password", sms_password);
            parameters.put("sender", sms_sender);

            parameters.put("msisdn", sms.getRecipient());
            parameters.put("message", sms.getMessage());
            parameters.put("division", sms.getDivision());
            parameters.put("batchname", sms.getBatchname());
            parameters.put("uploadby", sms.getUploadBy());
            parameters.put("channel", sms.getChannel().toString());

            con.setDoOutput(true);
            DataOutputStream out = new DataOutputStream(con.getOutputStream());
            String paramString = getParamsString(parameters);
            out.writeBytes(paramString);
            out.flush();
            out.close();

            int status = con.getResponseCode();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();

            System.out.println(content);
            con.disconnect();
            String[] responseSms = content.toString().split("=");

            System.out.println(responseSms[1]);
            if (responseSms[1].toString().equals("1") ) {
                sms.setStatus(AppConstant.SUCCESS);
            } else {
                sms.setStatus(AppConstant.FAILED);
            }
            sms.setResponses(content.toString());

        } catch (IOException e) {
            e.printStackTrace();
            sms.setStatus(AppConstant.FAILED);
            sms.setResponses(e.getMessage());
        }


        sms.setUpdatedAt(new Date());
        smsRepository.save(sms);
    }

    public static String getParamsString(Map<String, String> params)
            throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            result.append("&");
        }

        String resultString = result.toString();
        return resultString.length() > 0
                ? resultString.substring(0, resultString.length() - 1)
                : resultString;
    }
}
