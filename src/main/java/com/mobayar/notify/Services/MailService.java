package com.mobayar.notify.Services;


import com.mobayar.notify.Models.Email;
import com.mobayar.notify.Repositories.EmailRepository;
import com.mobayar.notify.Utils.AppConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Date;


@RefreshScope
@Service
public class MailService {

    private JavaMailSender javaMailSender;

    @Value("${spring.mail.from.email}")
    private String fromEmail;

    @Value("${spring.mail.from.name}")
    private String fromName;

    @Autowired
    private EmailRepository emailRepository;

    @Autowired
    public MailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Async
    public void send(String id) throws MailException {

        Email email = emailRepository.findById(id).get();

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(mimeMessage, false, "utf-8");

            mimeMessage.setContent(email.getBody(), "text/html");

            String[] to = email.getRecipient().split(",");
            helper.setTo(to);
            helper.setSubject(email.getSubject());
            helper.setFrom(fromEmail, fromName);

        } catch (MessagingException e) {
            email.setStatus(AppConstant.FAILED);
            email.setResponses(e.getMessage());
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            email.setStatus(AppConstant.FAILED);
            email.setResponses(e.getMessage());
            e.printStackTrace();
        }


        try {
            javaMailSender.send(mimeMessage);
            email.setStatus(AppConstant.SUCCESS);

        } catch (MailException e) {
            email.setStatus(AppConstant.FAILED);
            email.setResponses(e.getMessage());
            // runtime exception; compiler will not force you to handle it
        }

        if (email != null) {
            email.setUpdatedAt(new Date());
            emailRepository.save(email);
        }
    }

}