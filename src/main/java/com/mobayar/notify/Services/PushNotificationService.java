package com.mobayar.notify.Services;

import com.mobayar.notify.Models.PushNotification;
import com.mobayar.notify.Repositories.PushNotificationRepository;
import com.mobayar.notify.Utils.AppConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

@Service
public class PushNotificationService {

    @Value("${onesignal.api_key}")
    private String onesignal_api_key;

    @Value("${onesignal.app_id}")
    private String onesignal_app_id;

    @Autowired
    private PushNotificationRepository pushNotificationRepository;

    @Async
    public void send(String id) {

        PushNotification pushNotification = pushNotificationRepository.findById(id).get();

        try {
            String jsonResponse;

            URL url = new URL("https://onesignal.com/api/v1/notifications");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setUseCaches(false);
            con.setDoOutput(true);
            con.setDoInput(true);

            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("Authorization", "Basic " + onesignal_api_key);
            con.setRequestMethod("POST");

            String recipient = pushNotification.getRecipient();
            String recipient_target= "\"include_player_ids\": [\"" + recipient+ "\"],";

            String[] segments = {"All", "Active Users", "Inactive Users"};
            boolean found = Arrays.asList(segments).contains(recipient);
            if (found) {
                recipient_target="\"included_segments\": [\""+recipient+"\"],";
            }

            String strJsonBody = "{"
                    + "\"app_id\": \"" + onesignal_app_id + "\","
                    + recipient_target
                    +"\"data\": " + pushNotification.getData() + ","
                    + "\"headings\": {\"en\": \"" + pushNotification.getTitle() + "\"},"
                    + "\"contents\": {\"en\": \"" + pushNotification.getBody() + "\"},"
                    + "\"subtitle\": {\"en\": \"" + pushNotification.getTitle() + "\"}"
                    + "}";


//            System.out.println("strJsonBody:\n" + strJsonBody);

            byte[] sendBytes = strJsonBody.getBytes("UTF-8");
            con.setFixedLengthStreamingMode(sendBytes.length);

            OutputStream outputStream = con.getOutputStream();
            outputStream.write(sendBytes);

            pushNotification.setStatus(AppConstant.SUCCESS);
            int httpResponse = con.getResponseCode();
            System.out.println("httpResponse: " + httpResponse);

            jsonResponse = mountResponseRequest(con, httpResponse);
            System.out.println("jsonResponse:\n" + jsonResponse);
            pushNotification.setResponses(jsonResponse);

        } catch (Throwable t) {
            pushNotification.setStatus(AppConstant.FAILED);
            pushNotification.setResponses(t.getMessage());
            t.printStackTrace();
        }

        pushNotification.setUpdatedAt(new Date());
        pushNotificationRepository.save(pushNotification);
    }

    private static String mountResponseRequest(HttpURLConnection con, int httpResponse) throws IOException {
        String jsonResponse;
        if (httpResponse >= HttpURLConnection.HTTP_OK
                && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
            Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
            jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
            scanner.close();
        } else {
            Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
            jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
            scanner.close();
        }
        return jsonResponse;
    }

}
