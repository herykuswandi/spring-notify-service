package com.mobayar.notify.Repositories;

import com.mobayar.notify.Models.Email;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EmailRepository extends JpaRepository<Email, String> {

}

