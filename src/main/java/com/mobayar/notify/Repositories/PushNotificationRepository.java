package com.mobayar.notify.Repositories;


import com.mobayar.notify.Models.PushNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PushNotificationRepository extends JpaRepository<PushNotification, String> {

}

