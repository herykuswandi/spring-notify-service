package com.mobayar.notify.Repositories;

import com.mobayar.notify.Models.Sms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsRepository extends JpaRepository<Sms, String> {

}
